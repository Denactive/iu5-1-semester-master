const hashTableSize = 32;

class HashTable {
  constructor() {
    this.buckets = Array(hashTableSize).fill(null);
  }

  hash(key) {
    let hash = Array.from(key).reduce((sum, key) => {
      return sum + key.charCodeAt(0);
    }, 0);
    return hash % hashTableSize;
  }

  set(key, value) {
    // вычисляем хеш для ключа
    let index = this.hash(key);

    // если для данного хеша еще нет списка, создаем
    if (!this.buckets[index]) {
      this.buckets[index] = new LinkedList();
    }

    let list = this.buckets[index];
    // проверяем, не добавлен ли ключ ранее
    let node = list.find((nodeValue) => {
      nodeValue.key === key;
    });

    if (node) {
      node.value.value = value; // обновляем значение для ключа
    } else {
      list.append({ key, value }); // добавляем новый элемент в конец списка
    }
  }

  get(key) {
    // вычисляем хеш для ключа
    let index = this.hash(key);
    // находим в массиве соответствующий список
    let list = this.buckets[index];

    if (!list) return undefined;

    // ищем в списке элемент с нужным ключом
    let node = list.find((nodeValue) => {
      return nodeValue.key === key;
    });

    if (node) return node.value.value;
    return undefined;
  }

  delete(key) {
    let index = this.hash(key);
    let list = this.buckets[index];

    if (!list) return;

    let node = list.find((nodeValue) => nodeValue.key === key);
    if (!node) return;

    list.delete(node.value);
  }
}