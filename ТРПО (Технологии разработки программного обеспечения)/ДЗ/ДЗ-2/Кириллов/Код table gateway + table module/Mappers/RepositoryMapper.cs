﻿using System.Data;
using Npgsql;
using System.Linq;
using System;

namespace TableModule.Mappers
{
    public class RepositoryMapper : MapperBase
    {
        public RepositoryMapper(string connectionString): base(connectionString, "\"REPOSITORIES\"")
        {
        }

        public int Create(string name)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"NAME\") VALUES ('{name}');";
            return cmd.ExecuteNonQuery();
        }

        public int Update(string name)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"UPDATE {_tableName} SET \"NAME\"='{name}';";
            return cmd.ExecuteNonQuery();
        }

        public DataRow FindByName(string name)
        {
            using (NpgsqlCommand connection = new NpgsqlCommand(_connectionString))
            {
                string filter = $"NAME = '{name}'";
                return ReadAll().Select(filter).FirstOrDefault();
            }

        }
    }
}
