﻿using System.Data;
using Npgsql;
using System.Linq;

namespace TableModule.Mappers
{
    public abstract class MapperBase
    {
        protected string _connectionString;
        protected string _tableName;

        public MapperBase(string connectionString, string tableName)
        {
            _connectionString = connectionString;
            _tableName = tableName;
        }

        public DataTable ReadAll()
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                NpgsqlDataAdapter da = new NpgsqlDataAdapter($"SELECT * FROM {_tableName}", _connectionString);
                DataTable table = new DataTable();
                da.Fill(table);
                return table;
            }
        }

        public DataRow Read(int id)
        {
            string filter = $"ID = {id}";
            return ReadAll().Select(filter).FirstOrDefault();
        }

        public virtual int Create()
        {
            return 0;
        }

        public virtual int Update(int id)
        {
            return 0;
        }

        public int Delete(int id)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"DELETE FROM {_tableName} WHERE \"ID\"={id}";
            return cmd.ExecuteNonQuery();
        }

        public override string ToString()
        {
            var table = ReadAll();
            return Utils.ColumnNamesToString(table) + '\n' + Utils.DataTableToString(table);
        }
    }
}
