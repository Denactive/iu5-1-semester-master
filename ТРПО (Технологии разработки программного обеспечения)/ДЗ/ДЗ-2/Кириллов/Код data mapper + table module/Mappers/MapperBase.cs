﻿using System.Data;
using Npgsql;
using System.Linq;
using System;
using TableModule.Entities;

namespace TableModule.Mappers
{
    public abstract class MapperBase<Entity> where Entity: EntityBase
    {
        protected string _connectionString;
        protected string _tableName;
        protected Func<DataRow, Entity> _ctor;

        public MapperBase(string connectionString, string tableName, Func<DataRow, Entity> ctor)
        {
            _connectionString = connectionString;
            _tableName = tableName;
            _ctor = ctor;
        }

        protected DataTable _ReadAll()
        {
            using NpgsqlConnection connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            NpgsqlDataAdapter da = new NpgsqlDataAdapter($"SELECT * FROM {_tableName}", _connectionString);
            DataTable table = new DataTable();
            da.Fill(table);
            return table;
        }

        public Entity[] ReadAll()
        {
            var table = _ReadAll();
            return DataTable2Entities(table);
        }

        protected DataRow _Read(int id)
        {
            string filter = $"ID = {id}";
            return _ReadAll().Select(filter).FirstOrDefault();
        }

        public Entity Read(int id)
        {
            return DataRow2Entity(_Read(id));
        }

        public Entity DataRow2Entity(DataRow dr)
        {
            var en = _ctor(dr);
            en.Init();
            return en;
        }

        public Entity[] DataTable2Entities(DataTable table)
        {
            var rows = new Entity[table.Rows.Count];
            for (int i = 0; i < rows.Length; i++)
            {
                rows[i] = _ctor(table.Rows[i]);
                rows[i].Init();
            }
            return rows;
        }

        public virtual int Create(Entity en)
        {
            return 0;
        }

        public virtual int Update(Entity en)
        {
            return 0;
        }

        public virtual int Delete(Entity en)
        {
            return 0;
        }

        public override string ToString()
        {
            var table = _ReadAll();
            return Utils.ColumnNamesToString(table) + '\n' + Utils.DataTableToString(table);
        }
    }
}
