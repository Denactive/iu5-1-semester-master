/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 15.1 		*/
/*  Created On : 05-дек-2023 19:57:39 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */
/* Drop Sequences for Autonumber Columns */

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

/* Drop Tables */

DROP TABLE IF EXISTS "AirplanePlaces" CASCADE
;

DROP TABLE IF EXISTS "FlightLuggageVariants" CASCADE
;

DROP TABLE IF EXISTS "Flights" CASCADE
;

DROP TABLE IF EXISTS "LuggageVariants" CASCADE
;

DROP TABLE IF EXISTS "Passengers" CASCADE
;

DROP TABLE IF EXISTS "Tariffs" CASCADE
;

DROP TABLE IF EXISTS "Tickets" CASCADE
;

DROP TABLE IF EXISTS "Users" CASCADE
;

/* Create Tables */

CREATE TABLE "AirplanePlaces"
(
	"airplanePlacesID" bigserial NOT NULL,
	"businessMiddleRowsCount" integer NULL,
	"businessPlacesCount" integer NULL,
	"businessSideRowsCount" integer NULL,
	"economMiddleRowsCount" integer NULL,
	"economPlacesCount" integer NULL,
	"economSideRowsCount" integer NULL,
	"firstClassMiddleRowsCount" integer NULL,
	"firstClassPlacesCount" integer NULL,
	"firstClassSideRowsCount" integer NULL
)
;

CREATE TABLE "FlightLuggageVariants"
(
	"flightLuggageVariantsID" bigserial NOT NULL,
	"flightID" bigserial NULL,
	"luggageVariantID" bigserial NULL
)
;

CREATE TABLE "Flights"
(
	"flightID" bigserial NOT NULL,
	"departurePoint" varchar(50) NULL,
	"departureTimeAndDate" timestamp without time zone NULL,
	"destinantionPoint" varchar(50) NULL,
	"destionationTimeAndDate" timestamp without time zone NULL,
	company varchar(50) NULL,
	"airplanePlacesID" bigserial NULL,
	"userID" bigserial NULL
)
;

CREATE TABLE "LuggageVariants"
(
	"luggageVariantID" bigserial NOT NULL,
	cost real NULL,
	"maxWeight" integer NULL
)
;

CREATE TABLE "Passengers"
(
	"passengerID" bigserial NOT NULL,
	name varchar(50) NULL,
	surname varchar(50) NULL,
	gender boolean NULL,
	"dateBirthday" timestamp without time zone NULL,
	country varchar(50) NULL,
	"documentType" varchar(50) NULL,
	"documentNumber" varchar(50) NULL,
	"documentSeries" varchar(50) NULL,
	"userID" bigserial NOT NULL
)
;

CREATE TABLE "Tariffs"
(
	"tariffID" integer NOT NULL,
	class varchar(50) NULL,
	cost real NULL,
	eat boolean NULL,
	"hasChoiceOfPlace" boolean NULL,
	"luggageMaxWeight" integer NULL,
	name varchar(50) NULL,
	returnable boolean NULL,
	"flightID" bigserial NULL
)
;

CREATE TABLE "Tickets"
(
	"ticketID" bigserial NOT NULL,
	purchased boolean NULL,
	booked boolean NULL,
	"flightID" bigserial NULL,
	"luggageVariantID" bigserial NULL,
	"passengerID" bigserial NULL,
	"tariffID" integer NOT NULL
)
;

CREATE TABLE "Users"
(
	"userID" bigserial NOT NULL,
	login varchar(50) NULL,
	password varchar(50) NULL,
	"isAdmin" boolean NULL
)
;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE "AirplanePlaces" ADD CONSTRAINT "PK_AirplanePlaces"
	PRIMARY KEY ("airplanePlacesID")
;

ALTER TABLE "FlightLuggageVariants" ADD CONSTRAINT "PK_FlightLuggageVariants"
	PRIMARY KEY ("flightLuggageVariantsID")
;

ALTER TABLE "Flights" ADD CONSTRAINT "PK_Flights"
	PRIMARY KEY ("flightID")
;

ALTER TABLE "LuggageVariants" ADD CONSTRAINT "PK_LuggageVariants"
	PRIMARY KEY ("luggageVariantID")
;

ALTER TABLE "Passengers" ADD CONSTRAINT "PK_Passengers"
	PRIMARY KEY ("passengerID")
;

ALTER TABLE "Tariffs" ADD CONSTRAINT "PK_Tariffs"
	PRIMARY KEY ("tariffID")
;

ALTER TABLE "Tickets" ADD CONSTRAINT "PK_Tickets"
	PRIMARY KEY ("ticketID")
;

ALTER TABLE "Users" ADD CONSTRAINT "PK_Users"
	PRIMARY KEY ("userID")
;

/* Create Foreign Key Constraints */

ALTER TABLE "FlightLuggageVariants" ADD CONSTRAINT "FK_Flights_FlightLuggageVariants"
	FOREIGN KEY ("flightID") REFERENCES "Flights" ("flightID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "FlightLuggageVariants" ADD CONSTRAINT "FK_LuggageVariants_FlightLuggageVariants"
	FOREIGN KEY ("luggageVariantID") REFERENCES "LuggageVariants" ("luggageVariantID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Flights" ADD CONSTRAINT "FK_AirplanePlaces_Flights"
	FOREIGN KEY ("airplanePlacesID") REFERENCES "AirplanePlaces" ("airplanePlacesID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Flights" ADD CONSTRAINT "FK_Users_Flights"
	FOREIGN KEY ("userID") REFERENCES "Users" ("userID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Passengers" ADD CONSTRAINT "FK_Users_Passengers"
	FOREIGN KEY ("userID") REFERENCES "Users" ("userID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tariffs" ADD CONSTRAINT "FK_Flights_Tariffs"
	FOREIGN KEY ("flightID") REFERENCES "Flights" ("flightID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Flights_Tickets"
	FOREIGN KEY ("flightID") REFERENCES "Flights" ("flightID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_LuggageVariants_Tickets"
	FOREIGN KEY ("luggageVariantID") REFERENCES "LuggageVariants" ("luggageVariantID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Passengers_Tickets"
	FOREIGN KEY ("passengerID") REFERENCES "Passengers" ("passengerID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Tariffs_Tickets"
	FOREIGN KEY ("tariffID") REFERENCES "Tariffs" ("tariffID") ON DELETE No Action ON UPDATE No Action
;

/* Create Table Comments, Sequences for Autonumber Columns */

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
