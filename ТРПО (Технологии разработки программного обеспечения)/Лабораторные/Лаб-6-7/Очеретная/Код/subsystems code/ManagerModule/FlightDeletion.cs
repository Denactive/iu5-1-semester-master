package AirTickets.ManagerModule;

import AirTickets.AuthorizationModule.Authorization;
import AirTickets.SearchModule.FlightSearch;
import AirTickets.DatabaseModule.Flight;

/**
 * @author Pocht
 * @version 1.0
 * @created 03-���-2023 22:43:39
 */
public class FlightDeletion {

	public Authorization m_Authorization;
	public FlightSearch m_FlightSearch;
	public Flight m_Flight;

	public FlightDeletion(){

	}

	public void finalize() throws Throwable {

	}
	/**
	 * 
	 * @param flightId
	 */
	public void processDeleteFlightEvent(int flightId){

	}

	public void sendConfirmationMessageForDelete(){

	}
}//end FlightDeletion