﻿using System.Data;
using Npgsql;
using System.Linq;

namespace TableModule.Mappers
{
    public class UserMapper: MapperBase
    {
        public UserMapper(string connectionString): base(connectionString, "\"USERS\"")
        {
        }

        public int Create(string login, string password, string fio)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"LOGIN\", \"PASSWORD\", \"FIO\") VALUES ('{login}', '{password}', '{fio}');";
            return cmd.ExecuteNonQuery();
        }

        public int Update(int id, string login, string password, string fio)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"UPDATE {_tableName}" +
            $" SET \"LOGIN\"='{login}'," +
            $" \"PASSWORD\"='{password}'," +
            $" \"FIO\"='{fio}'" +
            $" WHERE id = {id}";
            return cmd.ExecuteNonQuery();
        }

        public DataRow FindByLogin(string login)
        {
            using (NpgsqlCommand connection = new NpgsqlCommand(_connectionString))
            {
                string filter = $"LOGIN = '{login}'";
                return ReadAll().Select(filter).FirstOrDefault();
            }

        }
    }
}
