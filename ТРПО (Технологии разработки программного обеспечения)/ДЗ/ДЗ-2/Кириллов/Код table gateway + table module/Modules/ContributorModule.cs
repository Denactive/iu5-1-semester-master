﻿using Npgsql;
using System;
using System.Data;
using System.Linq;

namespace TableModule.Modules
{
    public class ContributorModule : ModuleBase<Mappers.ContributorMapper>
    {
        public ContributorModule(string connectionString) : base(new Mappers.ContributorMapper(connectionString)) { }
        
        public int Create(int roleId, int userId)
        {
            return _mapper.Create(roleId, userId);
        }

        public DataRow Find(int contributorId)
        {
            return _mapper.Read(contributorId);
        }

        public int Delete(int contributorId)
        {
            return _mapper.Delete(contributorId);
        }
    }
}
