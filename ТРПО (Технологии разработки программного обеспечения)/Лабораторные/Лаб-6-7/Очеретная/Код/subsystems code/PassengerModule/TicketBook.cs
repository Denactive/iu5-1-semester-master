package AirTickets.PassengerModule;

import AirTickets.AuthorizationModule.Authorization;
import AirTickets.SearchModule.TicketSearch;
import AirTickets.DatabaseModule.Ticket;

/**
 * @author Pocht
 * @version 1.0
 * @created 03-���-2023 22:43:47
 */
public class TicketBook {

	public Authorization m_Authorization;
	public TicketSearch m_TicketSearch;
	public Ticket m_Ticket;

	public TicketBook(){

	}

	public void finalize() throws Throwable {

	}
	/**
	 * 
	 * @param ticketID
	 * @param luggageID
	 * @param tariffID
	 * @param place
	 */
	public bool checkTicketData(int ticketID, int luggageID, int tariffID, string place){
		return null;
	}

	/**
	 * 
	 * @param ticketID
	 */
	public void processCancelBookingTicketEvent(int ticketID){

	}

	/**
	 * 
	 * @param ticketID
	 * @param luggageID
	 * @param tariffID
	 * @param place
	 */
	public void processTicketDataForBook(int ticketID, int luggageID, int tariffID, string place){

	}
}//end TicketBook