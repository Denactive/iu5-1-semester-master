﻿using System.Data;

namespace TableModule.Entities
{
    public class User : EntityBase
    {
        public int id = -1;
        public string login = "";
        public string password = "";
        public string fio = "";

        public User() : base() {}

        public User(DataRow userDataRow) : base()
        {
            id = userDataRow.Field<int>("ID");
            login = userDataRow.Field<string>("LOGIN");
            password = userDataRow.Field<string>("PASSWORD");
            fio = userDataRow.Field<string>("FIO");
            Init();
        }

        public override string ToString()
        {
            return $"[User] {id} {login} {password} {fio}";
        }

        public User Copy()
        {
            var copy = new User();
            copy.id = this.id;
            copy.login = this.login;
            copy.password = this.password;
            copy.fio = this.fio;
            copy.status = this.status;
            return copy;
        }
    }
}
