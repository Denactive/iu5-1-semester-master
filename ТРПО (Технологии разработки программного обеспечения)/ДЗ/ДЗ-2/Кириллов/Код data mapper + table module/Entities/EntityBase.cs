﻿using System.Data;

namespace TableModule.Entities
{
    public class EntityBase
    {
        public string status = "idle";

        public EntityBase() {}

        public EntityBase(DataRow dr) { }

        public void Init()
        {
            status = "created";
        }

        public void Delete()
        {
            status = "deleted";
        }
    }
}
