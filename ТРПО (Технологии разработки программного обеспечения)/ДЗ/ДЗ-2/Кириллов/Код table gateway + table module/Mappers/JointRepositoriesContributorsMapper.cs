﻿using System.Data;
using Npgsql;
using System.Linq;
using System;

namespace TableModule.Mappers
{
    public class JointRepositoriesContributorsMapper : MapperBase
    {
        public JointRepositoriesContributorsMapper(string connectionString): base(connectionString, "\"REPOSITORIES_CONTRIBUTORS\"")
        {
        }

        public int Create(int repoID, int contributorId)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"INSERT INTO {_tableName} (\"CONTRIBUTORS_ID\", \"REPOSITORIES_ID\") VALUES ('{contributorId}', '{repoID}');";
            return cmd.ExecuteNonQuery();
        }

        public new int Read(int id)
        {
            Console.WriteLine($"У {_tableName} составной id. Пользуйтесь методом-перегрузкой");
            return 0;
        }

        public DataRow Read(int repoID, int contributorId)
        {
            string filter = $"CONTRIBUTORS_ID = {contributorId} AND REPOSITORIES_ID = {repoID}";
            return ReadAll().Select(filter).FirstOrDefault();
        }

        public new int Delete(int id)
        {
            Console.WriteLine($"У {_tableName} составной id. Пользуйтесь методом-перегрузкой");
            return 0;
        }

        public int Delete(int repoID, int contributorId)
        {
            var connection = new NpgsqlConnection(_connectionString);
            connection.Open();
            var cmd = new NpgsqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = $"DELETE FROM {_tableName} WHERE \"CONTRIBUTORS_ID\" = {contributorId} AND \"REPOSITORIES_ID\" = {repoID}";
            return cmd.ExecuteNonQuery();
        }

        public int[] GetRepositoryContributors(int repoID)
        {
            string filter = $"REPOSITORIES_ID = {repoID}";
            var table = this.ReadAll();
            var dataRows = table.Select(filter);

            int[] contributors = new int[dataRows.Length];

            for (int i = 0; i < contributors.Length; i++)
            {
                contributors[i] = dataRows[i].Field<int>("CONTRIBUTORS_ID");
            }

            return contributors;
        }
    }
}
