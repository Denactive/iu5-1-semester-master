﻿using Npgsql;
using System;
using System.Data;
using System.Linq;

namespace TableModule.Modules
{
    public class JointRepositoriesContributorsModule : ModuleBase<Mappers.JointRepositoriesContributorsMapper>
    {
        public JointRepositoriesContributorsModule(string connectionString) : base(new Mappers.JointRepositoriesContributorsMapper(connectionString)) { }
        
        public int Create(int repoID, int contributorId)
        {
            return _mapper.Create(repoID, contributorId);
        }

        public int Delete(int repoID, int contributorId)
        {
            return _mapper.Delete(repoID, contributorId);
        }

        public int[] GetRepositoryContributors(int repoID)
        {
            return _mapper.GetRepositoryContributors(repoID);
        }
    }
}
