package AirTickets.PassengerModule;

import AirTickets.AuthorizationModule.Authorization;
import AirTickets.DatabaseModule.Ticket;

/**
 * @author Pocht
 * @version 1.0
 * @created 03-���-2023 22:43:47
 */
public class TicketPurchase {

	public Authorization m_Authorization;
	public Ticket m_Ticket;
	public TicketBook m_TicketBook;

	public TicketPurchase(){

	}

	public void finalize() throws Throwable {

	}
	/**
	 * 
	 * @param number
	 * @param date
	 * @param csv
	 */
	public bool checkBankCardData(int number, string date, int csv){
		return null;
	}

	/**
	 * 
	 * @param ticketID
	 * @param luggageID
	 * @param tariffID
	 * @param place
	 */
	public bool checkTicketData(int ticketID, int luggageID, int tariffID, string place){
		return null;
	}

	/**
	 * 
	 * @param code
	 */
	public boolean checkVerificationCode(int code){
		return false;
	}

	/**
	 * 
	 * @param number
	 * @param date
	 * @param csv
	 */
	public void processBankCardData(int number, string date, int csv){

	}

	/**
	 * 
	 * @param ticketID
	 * @param luggageID
	 * @param tariffID
	 * @param place
	 */
	public void processTicketDataForPurchase(int ticketID, int luggageID, int tariffID, string place){

	}

	/**
	 * 
	 * @param code
	 */
	public void processVerificationCode(int code){

	}
}//end TicketPurchase