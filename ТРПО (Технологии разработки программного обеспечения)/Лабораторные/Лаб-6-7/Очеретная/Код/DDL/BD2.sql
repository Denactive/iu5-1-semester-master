/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 15.1 		*/
/*  Created On : 05-дек-2023 20:00:05 				*/
/*  DBMS       : PostgreSQL 						*/
/* ---------------------------------------------------- */

/* Drop Sequences for Autonumber Columns */

 

 

 

 

 

 

 

/* Drop Tables */

DROP TABLE IF EXISTS "AirplanePlaces" CASCADE
;

DROP TABLE IF EXISTS "FlightLuggageVariants" CASCADE
;

DROP TABLE IF EXISTS "Flights" CASCADE
;

DROP TABLE IF EXISTS "LuggageVariants" CASCADE
;

DROP TABLE IF EXISTS "Passengers" CASCADE
;

DROP TABLE IF EXISTS "Tariffs" CASCADE
;

DROP TABLE IF EXISTS "Tickets" CASCADE
;

DROP TABLE IF EXISTS "Users" CASCADE
;

/* Create Tables */

CREATE TABLE "AirplanePlaces"
(
	"airplanePlacesID" serial NOT NULL,
	"businessMiddleRowsCount" integer NOT NULL   DEFAULT 0,
	"businessPlacesCount" integer NOT NULL   DEFAULT 0,
	"businessSideRowsCount" integer NOT NULL   DEFAULT 0,
	"economMiddleRowsCount" integer NOT NULL   DEFAULT 0,
	"economPlacesCount" integer NOT NULL   DEFAULT 0,
	"economSideRowsCount" integer NOT NULL   DEFAULT 0,
	"firstClassMiddleRowsCount" integer NOT NULL   DEFAULT 0,
	"firstClassPlacesCount" integer NOT NULL   DEFAULT 0,
	"firstClassSideRowsCount" integer NOT NULL   DEFAULT 0
)
;

CREATE TABLE "FlightLuggageVariants"
(
	"flightLuggageVariantsID" serial NOT NULL,
	"flightID" integer NOT NULL,
	"luggageVariantID" integer NOT NULL
)
;

CREATE TABLE "Flights"
(
	"flightID" serial NOT NULL,
	"airplanePlacesID" integer NOT NULL,
	"departurePoint" varchar(255) NOT NULL,
	"departureTimeAndDate" timestamp without time zone NOT NULL,
	"destinantionPoint" varchar(255) NOT NULL,
	"destionationTimeAndDate" timestamp without time zone NOT NULL,
	company varchar(255) NOT NULL,
	"userID" integer NULL
)
;

CREATE TABLE "LuggageVariants"
(
	"luggageVariantID" serial NOT NULL,
	cost real NOT NULL,
	"maxWeight" integer NOT NULL   DEFAULT 0
)
;

CREATE TABLE "Passengers"
(
	"passengerID" serial NOT NULL,
	"userID" integer NOT NULL,
	name varchar(255) NOT NULL,
	surname varchar(255) NOT NULL,
	gender boolean NOT NULL   DEFAULT false,
	"dateBirthday" timestamp without time zone NOT NULL,
	country varchar(255) NOT NULL,
	"documentType" varchar(255) NOT NULL,
	"documentNumber" varchar(50) NOT NULL,
	"documentSeries" varchar(50) NOT NULL
)
;

CREATE TABLE "Tariffs"
(
	class varchar(100) NOT NULL,
	cost real NOT NULL,
	eat boolean NOT NULL   DEFAULT false,
	"hasChoiceOfPlace" boolean NOT NULL   DEFAULT false,
	"luggageMaxWeight" integer NOT NULL   DEFAULT 0,
	name varchar(100) NOT NULL,
	returnable boolean NOT NULL   DEFAULT false,
	"tariffID" integer NOT NULL,
	"flightID" integer NULL
)
;

CREATE TABLE "Tickets"
(
	"ticketID" serial NOT NULL,
	"flightID" integer NOT NULL,
	"luggageVariantID" integer NULL,
	"tariffID" integer NULL,
	purchased boolean NOT NULL   DEFAULT false,
	booked boolean NOT NULL   DEFAULT false,
	"passengerID" integer NULL
)
;

CREATE TABLE "Users"
(
	"userID" serial NOT NULL,
	login varchar(255) NOT NULL,
	password varchar(255) NOT NULL,
	"isAdmin" boolean NOT NULL   DEFAULT false
)
;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE "AirplanePlaces" ADD CONSTRAINT "PK_AirplanePlaces"
	PRIMARY KEY ("airplanePlacesID")
;

ALTER TABLE "FlightLuggageVariants" ADD CONSTRAINT "PK_FlightLuggageVariants"
	PRIMARY KEY ("flightLuggageVariantsID")
;

ALTER TABLE "Flights" ADD CONSTRAINT "PK_Flight"
	PRIMARY KEY ("flightID")
;

ALTER TABLE "LuggageVariants" ADD CONSTRAINT "PK_LuggageVariant"
	PRIMARY KEY ("luggageVariantID")
;

ALTER TABLE "Passengers" ADD CONSTRAINT "PK_User"
	PRIMARY KEY ("passengerID")
;

ALTER TABLE "Tariffs" ADD CONSTRAINT "PK_Tariff"
	PRIMARY KEY ("tariffID")
;

ALTER TABLE "Tickets" ADD CONSTRAINT "PK_Ticket"
	PRIMARY KEY ("ticketID")
;

ALTER TABLE "Users" ADD CONSTRAINT "PK_User"
	PRIMARY KEY ("userID")
;

/* Create Foreign Key Constraints */

ALTER TABLE "FlightLuggageVariants" ADD CONSTRAINT "FK_FlightLuggageVariants_Flights"
	FOREIGN KEY ("flightID") REFERENCES "Flights" ("flightID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "FlightLuggageVariants" ADD CONSTRAINT "FK_FlightLuggageVariants_LuggageVariants"
	FOREIGN KEY ("luggageVariantID") REFERENCES "LuggageVariants" ("luggageVariantID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Flights" ADD CONSTRAINT "FK_Flights_AirplanePlaces"
	FOREIGN KEY ("airplanePlacesID") REFERENCES "AirplanePlaces" ("airplanePlacesID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Flights" ADD CONSTRAINT "FK_Flights_Users"
	FOREIGN KEY ("userID") REFERENCES "Users" ("userID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Passengers" ADD CONSTRAINT "FK_Passengers_Users"
	FOREIGN KEY ("userID") REFERENCES "Users" ("userID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tariffs" ADD CONSTRAINT "FK_Tariffs_Flights"
	FOREIGN KEY ("flightID") REFERENCES "Flights" ("flightID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Tickets_Flights"
	FOREIGN KEY ("flightID") REFERENCES "Flights" ("flightID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Tickets_LuggageVariants"
	FOREIGN KEY ("luggageVariantID") REFERENCES "LuggageVariants" ("luggageVariantID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Tickets_Passengers"
	FOREIGN KEY ("passengerID") REFERENCES "Passengers" ("passengerID") ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "Tickets" ADD CONSTRAINT "FK_Tickets_Tariffs"
	FOREIGN KEY ("tariffID") REFERENCES "Tariffs" ("tariffID") ON DELETE No Action ON UPDATE No Action
;

/* Create Table Comments, Sequences for Autonumber Columns */

 

 

 

 

 

 

 
